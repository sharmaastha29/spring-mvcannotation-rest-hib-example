package com.astha.dao;

import java.util.List;

import com.astha.model.Product;

public interface IProductdao {
	//All methds in interface are public and abstract by default so even if you don't put public it will be public and abstract
	public List<Product> getAllProducts();
	public Product getSpecificProduct(int id);
	public int createProduct(Product p);
	public Product updateProduct(int id, Product p);
	public int deleteProduct(int id);
}
