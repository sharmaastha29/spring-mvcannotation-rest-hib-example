package com.astha.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.astha.model.Product;



@Repository
@Transactional(readOnly=true)
public class ProductdaoImpl implements IProductdao {

@Autowired
private SessionFactory sf;

	public List<Product> getAllProducts(){
		Session s=sf.getCurrentSession();
			CriteriaBuilder cb=s.getCriteriaBuilder();
			CriteriaQuery<Product> cq=cb.createQuery(Product.class);
			Root<Product> r=cq.from(Product.class);
			cq.select(r);
			Query<Product> q=s.createQuery(cq);
			return q.getResultList();
	}
	
	
	public Product getSpecificProduct(int id){
		 Session s=sf.getCurrentSession();
		 return s.get(Product.class, id);
		
	}
	
	@Transactional
	public int createProduct(Product p){
		Session s=sf.getCurrentSession();
		s.save(p);
		return p.getId();
	}
	
	@Transactional
	public Product updateProduct(int id, Product p){
		Session s=sf.getCurrentSession();
		Product prd=s.byId(Product.class).load(id);
		prd.setName(p.getName());
		prd.setBrand(p.getBrand()); 
		s.flush();
		return prd;
	}
	
	@Transactional
	public int deleteProduct(int id){
		Session s=sf.getCurrentSession();
		//s.delete(object);// to del all records..table deletion
		Product prd=s.byId(Product.class).load(id);
		s.delete(prd);
		return prd.getId();	
	}
}
