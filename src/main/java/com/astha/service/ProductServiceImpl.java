package com.astha.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astha.dao.IProductdao;
import com.astha.model.Product;

@Service
public class ProductServiceImpl implements IProductService {
	
	@Autowired
	private IProductdao p;

	public List<Product> get(){
		return p.getAllProducts();
	}
	
	public Product getSpecific(int id){
		return p.getSpecificProduct(id);
	}
	
	public int create(Product prd){
		return p.createProduct(prd);
	}
	
	public Product update(int id, Product prd){
		return p.updateProduct(id, prd);
	}
	
	public int  delete(int id){
		return p.deleteProduct(id);
	}
}
