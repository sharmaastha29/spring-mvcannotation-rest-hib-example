package com.astha.service;

import java.util.List;

import com.astha.model.Product;

public interface IProductService {
	List<Product> get();
	Product getSpecific(int id);
	int create(Product prd);
	Product update(int id, Product prd);
	int  delete(int id);
}
